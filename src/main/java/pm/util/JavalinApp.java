package pm.util;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;
import pm.controllers.*;

public class JavalinApp {
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                    path("/enviarEmail", () -> post(EnviarEmailController::postEnviarEmail));
                    path("/cobranca", () -> post(CobrancaController::postCobranca));
                    path("/cobranca/:id", () -> get(CobrancaController::getCobranca));
                    path("/filaCobranca", () -> post(CobrancaController::postFilaCobranca));
                    path("/validaCartaoDeCredito", () -> post(ValidarCartaoDeCreditoController::postValidarCartaoCredito));
                    });


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
