package pm.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import io.javalin.http.Context;
import pm.models.Cobranca;
import pm.models.CobrancaStatus;
import pm.models.ContextStatus;
import pm.models.NovaCobranca;
import pm.services.CobrancaService;

public class CobrancaController {
	
	private CobrancaController() {
	}

	private static final String COBRANCA_SOLICITADA = "Cobrança solicitada";
	private static final String DADOS_INVALIDOS = "Dados inválidos";
	private static final String NAO_ENCONTRADO = "Não encontrado";
	
	public static void postCobranca(Context ctx) {
		
		NovaCobranca novaCobranca = ctx.bodyAsClass(NovaCobranca.class);
		CobrancaService cobrancaService = new CobrancaService();
		
		if(cobrancaService.validarCobranca(novaCobranca)) {
			
			Date data = new Date();
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			
			Cobranca cobranca = new Cobranca(UUID.randomUUID(), CobrancaStatus.PENDENTE, formato.format(data), novaCobranca);
			CobrancaService.adicionarCobranca(cobranca);
			
			ctx.status(200);
			ctx.json(cobranca);
			ctx.result(ContextStatus.contextStatus("200", COBRANCA_SOLICITADA));
		} else {
			ctx.status(422);
			ctx.result(ContextStatus.contextStatus("422", DADOS_INVALIDOS));
		}
	}
	
	public static void getCobranca(Context ctx) {
		String id = ctx.pathParam("id");
		
		Cobranca cobranca = CobrancaService.acharCobrancaPeloId(id);
		
		if(cobranca != null) {
			ctx.status(200);
			ctx.result(cobranca.toString());
		} else {
			ctx.status(404);
			ctx.result(ContextStatus.contextStatus("404", NAO_ENCONTRADO));
		}
	}
	
	public static void postFilaCobranca(Context ctx) {
		
		NovaCobranca novaCobranca = ctx.bodyAsClass(NovaCobranca.class);
		CobrancaService cobrancaService = new CobrancaService();
		
		if(cobrancaService.validarCobranca(novaCobranca)) {
			Date data = new Date();
			SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			
			Cobranca cobranca = new Cobranca(UUID.randomUUID(), CobrancaStatus.PENDENTE, formato.format(data), novaCobranca);
			cobrancaService.adicionarCobrancaFila(cobranca);
			
			ctx.status(200);
			ctx.json(cobranca);
			ctx.result(ContextStatus.contextStatus("200", COBRANCA_SOLICITADA));
			
		} else {
			ctx.status(422);
			ctx.result(ContextStatus.contextStatus("422", DADOS_INVALIDOS));
		}
	}
}
