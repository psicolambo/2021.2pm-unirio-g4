package pm.controllers;

import java.util.UUID;
import io.javalin.http.Context;
import pm.models.ContextStatus;
import pm.models.NovoCartaoDeCredito;
import pm.models.CartaoDeCredito;
import pm.services.CartaoDeCreditoService;

public class ValidarCartaoDeCreditoController {

	private ValidarCartaoDeCreditoController() {
	}
	
	private static final String DADOS_ATUALIZADOS = "Dados atualizados";
	private static final String NAO_ENCONTRADO = "Não encontrado";
	private static final String DADOS_INVALIDOS = "Dados inválidos";
	
	public static void postValidarCartaoCredito(Context ctx) {
		
		NovoCartaoDeCredito novoCartaoDeCredito = ctx.bodyAsClass(NovoCartaoDeCredito.class);
		
		CartaoDeCreditoService cartaoDeCreditoService = new CartaoDeCreditoService();
		
		if(cartaoDeCreditoService.preenchidoCartaoDeCredito(novoCartaoDeCredito)) {
			
			if(cartaoDeCreditoService.validarDataValidadeCartao(novoCartaoDeCredito) && cartaoDeCreditoService.validarCvv(novoCartaoDeCredito)) {
				
				CartaoDeCredito cartaoDeCredito = new CartaoDeCredito(UUID.randomUUID(), novoCartaoDeCredito);
				
				ctx.status(200);
				ctx.json(cartaoDeCredito);
				ctx.result(ContextStatus.contextStatus("200", DADOS_ATUALIZADOS));
				
			} else {
				
				ctx.status(422);
				ctx.result(ContextStatus.contextStatus("422", DADOS_INVALIDOS));
			}
			
		} else {
			
			ctx.status(404);
			ctx.result(ContextStatus.contextStatus("404", NAO_ENCONTRADO));
		}
	}
}
