package pm.controllers;

import java.util.UUID;

import io.javalin.http.Context;
import pm.models.ContextStatus;
import pm.models.Email;
import pm.models.NovoEmail;
import pm.services.EmailService;

public class EnviarEmailController {
	
	private EnviarEmailController() {
	}
	
	private static final String EXTERNO_SOLICITADA = "Externo solicitada";
	private static final String EMAIL_NAO_EXISTE = "E-mail não existe";
	private static final String EMAIL_FORMATO_INVALIDO = "E-mail com formato inválido";
	
	public static void postEnviarEmail(Context ctx) {
		
		NovoEmail novoEmail = ctx.bodyAsClass(NovoEmail.class);
		EmailService emailService = new EmailService();
		
		if(!novoEmail.getEmail().equals("")) {
			
			if(emailService.validarFormatoEmail(novoEmail.getEmail())) {
				
				Email email = new Email(UUID.randomUUID(), novoEmail);
				emailService.enviarEmail(email);
				
				ctx.status(200);
				ctx.json(email);
				ctx.result(ContextStatus.contextStatus("200", EXTERNO_SOLICITADA));
				
			} else {
				
				ctx.status(422);
				ctx.result(ContextStatus.contextStatus("422", EMAIL_FORMATO_INVALIDO));
			}
			
		} else {
			
			ctx.status(404);
			ctx.result(ContextStatus.contextStatus("404", EMAIL_NAO_EXISTE));
		}
	}
}
