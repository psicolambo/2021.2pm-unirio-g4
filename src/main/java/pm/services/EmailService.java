package pm.services;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.SimpleEmail;

import pm.models.Email;

public class EmailService {
	
	//Método que verifica se o formato do email é válido
	public boolean validarFormatoEmail(String email) {
		
		return (email.contains("@") && email.contains("."));
	}
	
	//Método que envia o email
	public void enviarEmail(Email email) {
		
		SimpleEmail simEmail = new SimpleEmail();
		simEmail.setHostName("smtp.gmail.com");
		simEmail.setSmtpPort(465);
		
		//email criado apenas para a disciplina 
		simEmail.setAuthenticator(new DefaultAuthenticator("pmbreno4@gmail.com", "4#3U4^DoKgpWKT"));
		simEmail.setSSLCheckServerIdentity(true);
		simEmail.setSSLOnConnect(true);
		
		try {
			simEmail.setFrom("pmbreno4@gmail.com");
			simEmail.setMsg(email.getNovoEmail().getMensagem());
			simEmail.addTo(email.getNovoEmail().getEmail());
			simEmail.send();
			
		} catch(Exception e) {
			 new Throwable().printStackTrace();
		}
	}
}
