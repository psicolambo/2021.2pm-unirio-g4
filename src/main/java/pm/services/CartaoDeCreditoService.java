package pm.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

import pm.models.NovoCartaoDeCredito;

public class CartaoDeCreditoService {

	public boolean preenchidoCartaoDeCredito(NovoCartaoDeCredito novoCartaoDeCredito) {
		return !novoCartaoDeCredito.getNomeTitular().equals("") && !novoCartaoDeCredito.getNumero().equals("") && !novoCartaoDeCredito.getValidade().equals("") && !novoCartaoDeCredito.getCvv().equals("");
	}
	
	public boolean validarDataValidadeCartao(NovoCartaoDeCredito novoCartaoDeCredito) {
		String formatoData = "uuuu-MM-dd";

	    DateTimeFormatter dateTimeFormatter = DateTimeFormatter
	    .ofPattern(formatoData)
	    .withResolverStyle(ResolverStyle.STRICT);
	    try {
	        LocalDate.parse(novoCartaoDeCredito.getValidade(), dateTimeFormatter);
	        return true;
	    } catch (DateTimeParseException e) {
	       return false;
	    } 
	}
	
	public boolean validarCvv(NovoCartaoDeCredito novoCartaoDeCredito) {
		return novoCartaoDeCredito.getCvv().length() == 3 && novoCartaoDeCredito.getCvv().matches("[0-9]*");
	}
}
