package pm.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import pm.models.NovaCobranca;
import pm.models.Cobranca;
import pm.models.CobrancaStatus;

public class CobrancaService {

	//HashMap feito para testar o getCobranca
	private static HashMap<UUID, Cobranca> cobrancas = new HashMap<>();
	private static HashMap<UUID, Cobranca> filaCobrancas = new HashMap<>();
	
	static String horaSolicitacao = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
	
	static {
		cobrancas.put(UUID.fromString("0f14d0ab-9605-4a62-a9e4-5ed26688389b"), new Cobranca(UUID.fromString("0f14d0ab-9501-4a62-a9e4-5ed26688389b"), CobrancaStatus.PENDENTE, horaSolicitacao, (new NovaCobranca(BigDecimal.ONE, UUID.fromString("0f14d0ab-9091-4a62-a9e4-5ed26688389b")))));
	}
	
	//Método que verifica se o formato do email é válido
	public boolean validarCobranca(NovaCobranca novaCobranca) {
		
		String idCiclista = novaCobranca.getCiclista().toString();

		return ((novaCobranca.getValor().compareTo(BigDecimal.ZERO) > 0) && !idCiclista.equals("") && !idCiclista.equals(" "));
	}
	
	public static Cobranca acharCobrancaPeloId(String cobrancaId) {
		return cobrancas.get(UUID.fromString(cobrancaId));
	}
	
	public static Cobranca adicionarCobranca(Cobranca cobranca) {
		return cobrancas.put(cobranca.getId(), cobranca);
	}
	
	public void adicionarCobrancaFila(Cobranca cobranca) {
		filaCobrancas.put(cobranca.getId(), cobranca);
	}
	
}
