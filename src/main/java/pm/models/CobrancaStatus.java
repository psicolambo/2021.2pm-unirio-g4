package pm.models;

public enum CobrancaStatus {
		
	PENDENTE("pendente"), 
	PAGA("paga"),
	FALHA("falha"), 
	CANCELADA("cancelada"), 
	OCUPADA("ocupafa");

	private String status;

	CobrancaStatus(String string) {
		this.status = string;
	}

	public String getStatus() {
		return status;
	}
}

