package pm.models;

public class NovoCartaoDeCredito {

	//Atributos
	private String nomeTitular;
	private String numero;
	private String validade;
	private String cvv;
	
	//Construtores
	public NovoCartaoDeCredito() {
	}
	
	public NovoCartaoDeCredito(String nomeTitular, String numero, String validade, String cvv) {
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
		this.cvv = cvv;
	}
	
	//Getters e Setters
	public String getNomeTitular() {
		return nomeTitular;
	}
	
	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getValidade() {
		return validade;
	}
	
	public void setValidade(String validade) {
		this.validade = validade;
	}
	
	public String getCvv() {
		return cvv;
	}
	
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
}
