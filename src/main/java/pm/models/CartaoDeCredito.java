package pm.models;

import java.util.UUID;

public class CartaoDeCredito {
	
	//Atributos
	private UUID id;
	private NovoCartaoDeCredito cartao;

	//Construtores
	public CartaoDeCredito() {
	}

	public CartaoDeCredito(UUID id, NovoCartaoDeCredito cartao) {
		this.id = id;
		this.cartao = cartao;
	}

	//Getters e Setters
	public NovoCartaoDeCredito getCartao() {
		return cartao;
	}

	public void setCvv(NovoCartaoDeCredito cartao) {
		this.cartao = cartao;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
