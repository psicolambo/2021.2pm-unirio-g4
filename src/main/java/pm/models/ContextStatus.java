package pm.models;

public class ContextStatus {
	
	private ContextStatus() {
	}
	
	public static String contextStatus(String codigo, String mensagem) { 
		return codigo + " - " + mensagem;
	}

}
