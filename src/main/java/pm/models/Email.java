package pm.models;

import java.util.UUID;

public class Email {

	//Atributos
	private UUID id;
	private NovoEmail novoEmail;
	
	//Construtores
	public Email(UUID id, NovoEmail novoEmail) {
		this.id = id;
		this.novoEmail = novoEmail;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public NovoEmail getNovoEmail() {
		return novoEmail;
	}

	public void setNovoEmail(NovoEmail novoEmail) {
		this.novoEmail = novoEmail;
	}
}
