package pm.models;

import java.math.BigDecimal;
import java.util.UUID;

public class NovaCobranca {
	//Atributos
	private BigDecimal valor;
	private UUID ciclista;

	//Construtores
	public NovaCobranca() {
	}

	public NovaCobranca(BigDecimal valor, UUID ciclista) {
		this.valor = valor;
		this.ciclista = ciclista;
	}
	
	//Getters e Setters
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public UUID getCiclista() {
		return ciclista;
	}

	public void setCiclista(UUID ciclista) {
		this.ciclista = ciclista;
	}
}
