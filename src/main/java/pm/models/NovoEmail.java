package pm.models;

public class NovoEmail {
	
	//Atributos
	private String email;
	private String mensagem;
	
	//Construtores
	public NovoEmail() {
	}
	
	public NovoEmail(String email, String mensagem) {
		this.email = email;
		this.mensagem = mensagem;
	}
	
	//Getters e Setters
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
