package pm.models;

import java.util.UUID;

public class Cobranca {
	
	//Atributos
	private UUID id;
	private CobrancaStatus status;
	private String horaSolicitacao;
	private String horaFinalizacao; 
	private NovaCobranca novaCobranca;

	//Construtores
	public Cobranca() {
	}

	public Cobranca(UUID id, CobrancaStatus status, String horaSolicitacao, NovaCobranca novaCobranca) {
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.novaCobranca = novaCobranca;
	}

	public Cobranca(UUID id, CobrancaStatus status, String horaSolicitacao, String horaFinalizacao, NovaCobranca novaCobranca) {
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.novaCobranca = novaCobranca;
	}

	//Getters e Setters
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public CobrancaStatus getStatus() {
		return status;
	}

	public void setStatus(CobrancaStatus status) {
		this.status = status;
	}

	public String getHoraSolicitacao() {
		return horaSolicitacao;
	}

	public void setHoraSolicitacao(String horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}

	public String getHoraFinalizacao() {
		return horaFinalizacao;
	}

	public void setHoraFinalizacao(String horaFinalizacao) {
		this.horaFinalizacao = horaFinalizacao;
	}

	public NovaCobranca getNovaCobranca() {
		return novaCobranca;
	}

	public void setNovaCobranca(NovaCobranca novaCobranca) {
		this.novaCobranca = novaCobranca;
	}
	
}
