package pm;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import pm.models.Cobranca;
import pm.models.NovaCobranca;
import pm.models.NovoCartaoDeCredito;
import pm.models.NovoEmail;
import pm.util.JavalinApp;

class ControllerTest {

    private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }
    
    @Test
    void postEnviarEmail() {
    	//200
    	NovoEmail email = new NovoEmail("breno.maia@uniriotec.br", "Olá");
    	HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/enviarEmail").body(email).asJson();
    	assertEquals(200, response.getStatus());
    	
    	//404
    	email = new NovoEmail("", "Olá");
    	response = Unirest.post("http://localhost:7010/enviarEmail").body(email).asJson();
    	assertEquals(404, response.getStatus());
    	
    	//422
    	email = new NovoEmail("breno@uniriotecbr", "Olá");
    	response = Unirest.post("http://localhost:7010/enviarEmail").body(email).asJson();
    	assertEquals(422, response.getStatus());
    	
    	email = new NovoEmail("brenouniriotec.br", "Olá");
    	response = Unirest.post("http://localhost:7010/enviarEmail").body(email).asJson();
    	assertEquals(422, response.getStatus());
    }
    
    @Test
    void postCobranca() {
    	//200
    	NovaCobranca novaCobranca = new NovaCobranca(BigDecimal.valueOf(2), UUID.randomUUID());
    	HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/cobranca").body(novaCobranca).asJson();
    	assertEquals(200, response.getStatus());
    	
    	//422
    	novaCobranca = new NovaCobranca(BigDecimal.valueOf(-1), UUID.randomUUID());
    	response = Unirest.post("http://localhost:7010/cobranca").body(novaCobranca).asJson();
    	assertEquals(422, response.getStatus());
    }
    
    @Test
    void postFilaCobranca() {
    	//200
    	NovaCobranca novaCobranca = new NovaCobranca(BigDecimal.valueOf(2), UUID.randomUUID());
    	HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/filaCobranca").body(novaCobranca).asJson();
    	assertEquals(200, response.getStatus());
    	
    	//422
    	novaCobranca = new NovaCobranca(BigDecimal.valueOf(-1), UUID.randomUUID());
    	response = Unirest.post("http://localhost:7010/filaCobranca").body(novaCobranca).asJson();
    	assertEquals(422, response.getStatus());
    }
    
    @Test
    void getCobranca() {
    	//200
    	HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cobranca/0f14d0ab-9605-4a62-a9e4-5ed26688389b").header("accept", "application/json").asJson();
    	assertEquals(200, response.getStatus());
    	
    	//404
    	response = Unirest.get("http://localhost:7010/cobranca/0f14d0ab-9605-4a62-a9e4-5ed26688389a").header("accept", "application/json").asJson();
    	assertEquals(404, response.getStatus());
    }
    
    @Test
    void postValidarCartaoDeCredito() {
    	//200
    	NovoCartaoDeCredito novoCartaoDeCredito = new NovoCartaoDeCredito("Fulano das Couves", "1111222233334444", "2027-10-01", "123");
    	HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/validaCartaoDeCredito").body(novoCartaoDeCredito).asJson();
    	assertEquals(200, response.getStatus());
    	
    	//404
    	novoCartaoDeCredito =  new NovoCartaoDeCredito("", "1111222233334444", "2027-10-01", "123");
    	response = Unirest.post("http://localhost:7010/validaCartaoDeCredito").body(novoCartaoDeCredito).asJson();
    	assertEquals(404, response.getStatus());
    	
    	novoCartaoDeCredito =  new NovoCartaoDeCredito("Fulano das Couves", "", "2027-10-01", "123");
    	response = Unirest.post("http://localhost:7010/validaCartaoDeCredito").body(novoCartaoDeCredito).asJson();
    	assertEquals(404, response.getStatus());
    	
    	novoCartaoDeCredito =  new NovoCartaoDeCredito("Fulano das Couves", "1111222233334444", "", "123");
    	response = Unirest.post("http://localhost:7010/validaCartaoDeCredito").body(novoCartaoDeCredito).asJson();
    	assertEquals(404, response.getStatus());
    	
    	novoCartaoDeCredito =  new NovoCartaoDeCredito("Fulano das Couves", "1111222233334444", "2027-10-01", "");
    	response = Unirest.post("http://localhost:7010/validaCartaoDeCredito").body(novoCartaoDeCredito).asJson();
    	assertEquals(404, response.getStatus());
    	
    	//422
    	novoCartaoDeCredito =  new NovoCartaoDeCredito("Fulano das Couves", "1111222233334444", "20-10-01", "123");
    	response = Unirest.post("http://localhost:7010/validaCartaoDeCredito").body(novoCartaoDeCredito).asJson();
    	assertEquals(422, response.getStatus());
    	
    	novoCartaoDeCredito =  new NovoCartaoDeCredito("Fulano das Couves", "1111222233334444", "2027-10-01", "1234");
    	response = Unirest.post("http://localhost:7010/validaCartaoDeCredito").body(novoCartaoDeCredito).asJson();
    	assertEquals(422, response.getStatus());
    }
    
}